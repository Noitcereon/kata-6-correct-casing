let kataNumber = 6;
let kataName = `Kata ${kataNumber}: Correct Casing (Game of Thrones: Character Titles)`;
document.title = kataName;
document.getElementById("frontPageH1").innerText = kataName;


function correctTitle(characterNameIncludingTitle) {
    if (typeof (characterNameIncludingTitle) !== "string") {
        console.warn("Incorrect input");
        return;
    }
    let correctedTitle = "";
    let preCorrectionTitle = characterNameIncludingTitle.toLowerCase().split(" ");

    const lastIndex = preCorrectionTitle.length - 1;
    for (let index = 0; index < preCorrectionTitle.length; index++) {
        let word = preCorrectionTitle[index];
        word.replace(" ", "");
        if (word.includes("-")) {
            // handle edge case
        }
        
        word += " "
        word = word.charAt(0).toUpperCase() + word.slice(1);
        correctedTitle += word;
        if (index === lastIndex) {
            // add dot at end if its not there.
        }
    }

    return correctedTitle;
}
const capitalize = (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
  }

console.log(correctTitle("jOn SnoW, kINg IN thE noRth")); // ➞ "Jon Snow, King in the North."
console.log(correctTitle("sansa stark,lady of winterfell.")); // ➞ "Sansa Stark, Lady of Winterfell."
console.log(correctTitle("TYRION LANNISTER, HAND OF THE QUEEN.")); // ➞ "Tyrion Lannister, Hand of the Queen."


// Make the string lowercase
// Split the string into words
// Loop through every word
    // if the word has a dash
        // split the word
        // capitalize index 0 of both words
        // concatonate them together again
    // if the word is a preposition
        // lowerCase it
// Capitalize the first letter (unless it is a preposition)
